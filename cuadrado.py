class Cuadrado: 
    """Un ejemplo de una clase para los Cuadrados"""
    def __init__(self, l=1):
        self.lado = l
        #self.area = self.lado**2

    def calculo_perimetro (self):
        return self.lado * 4

    def calculo_area (self):
        return self.lado * self.lado #area(para el caso de self.area. Para que funcionees necesario meterle un self)

    def __str__ (self): 
        return 


cuadrado01 = Cuadrado(2)
cuadrado02 = Cuadrado(3)

print(cuadrado01.calculo_perimetro()) #llamo a un método, no a una función.
print(cuadrado02.calculo_perimetro())
print(cuadrado01.calculo_area())
print(cuadrado02.calculo_area())