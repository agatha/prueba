from fraccion import Fraccion
import unittest

class TestFraccion(unittest.TestCase):
    def test_suma(self):
        f1 = Fraccion(1, 2)
        f2 = Fraccion(1, 2)
        suma = f1.suma (f2)
        self.assertEqual(suma, 1)
      

unittest.main()