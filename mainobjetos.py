class Empleado:
    def __init__(self, nombre, salario, tasa, edad, antiguedad):
        self.__nombre = nombre
        self.__salario = salario
        self.__tasa = tasa
        self.__edad = edad
        self.__antiguedad = antiguedad
    def CalculoImpuestos(self):
        self.__impuestos = self.__salario*self.__tasa
        print ("El empleado {name} debe pagar {tax:.2f}".format(name=self.__nombre, tax=self.__impuestos))
        return self.__impuestos
    def ImprimirEdad(self):
        print("Edad de " + self.__nombre + ":" + str(self.__edad))
    def ImprimirAntiguedad(self):
        print("Antiguedad de " + self.__nombre + ":" + str(self.__antiguedad))
    def ImpuestosRebajados(self):
        if self.__antiguedad > 1:
            self.__impuestos = self.__impuestos * 0.9
        print("Impuestos ya rebajados: " + str(self.__impuestos))
        return 
       
def displayCost(total):
    print("Los impuestos a pagar en total son {:.2f} euros".format(total))

emp1 = Empleado("Pepe", 20000, 0.35, 50, 0.8)
emp2 = Empleado("Ana", 30000, 0.30, 30, 0.2)
empleados = [emp1, emp2, Empleado("Luis", 10000, 0.10, 45, 10), Empleado("Luisa", 25000, 0.15, 70, 29)]
total = 0
for emp in empleados:
    emp.ImprimirEdad()
    total += emp.CalculoImpuestos()
    emp.ImprimirAntiguedad()
    emp.ImpuestosRebajados()
displayCost(total)

