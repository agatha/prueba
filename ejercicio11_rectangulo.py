class Rectangulo:
    def __init__(self, b, h):
        self.base = b
        self.altura = h

    def CalculoPerimetro(self):
        return self.base*2 + self.altura*2

    def CalculoArea(self):
        return (self.base*self.altura)

    def __str__(self):  
        return("El rectángulo de base {base} y altura {altura} tiene un perímetro de {perimetro} y un área de {area}" .format(base=self.base, altura=self.altura, area=self.CalculoArea(), perimetro=self.CalculoPerimetro()))

rectangulo01 = Rectangulo (2,3)
rectangulo02 = Rectangulo(4,5)
print(rectangulo01)
print(rectangulo02)







